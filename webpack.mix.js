const mix = require("laravel-mix");
const ESLintPlugin = require("eslint-webpack-plugin");

mix.js("resources/js/app.js", "public/js");
mix.postCss("resources/css/app.css", "public/css", [require("tailwindcss")]);
mix.postCss("resources/css/order.css", "public/css");
mix.webpackConfig({
    plugins: [
        new ESLintPlugin({
            extensions: ["js"],
        }),
    ],
});

if (mix.inProduction()) {
    mix.version();
}
