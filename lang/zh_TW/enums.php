<?php

use App\Enums;

return [
    Enums\OrderStatus::class => [
        Enums\OrderStatus::PENDING    => '待付款',
        Enums\OrderStatus::PROCESSING => '待處理',
        Enums\OrderStatus::SHIPPED    => '已出貨',
        Enums\OrderStatus::DELIVERED  => '已送達',
        Enums\OrderStatus::CANCELLED  => '已取消',
    ],
    Enums\OrderType::class => [
        Enums\OrderType::ONE_TIME     => '單次消費',
        Enums\OrderType::SUBSCRIPTION => '定期定額',
    ],
    Enums\PaymentMethod::class => [
        Enums\PaymentMethod::CREDIT  => '信用卡',
        Enums\PaymentMethod::ATM     => 'ATM',
        Enums\PaymentMethod::CVS     => '超商代碼',
        Enums\PaymentMethod::BARCODE => '超商條碼',
        Enums\PaymentMethod::WEB_ATM => '網路 ATM',
    ],
    Enums\PaymentStatus::class => [
        Enums\PaymentStatus::UNPAID => '未付款',
        Enums\PaymentStatus::PAID   => '已付款',
        Enums\PaymentStatus::FAILED => '付款失敗',
    ],
    Enums\SubscriptionStatus::class => [
        Enums\SubscriptionStatus::PENDING   => '待付款',
        Enums\SubscriptionStatus::ACTIVE    => '已啟用',
        Enums\SubscriptionStatus::CANCELLED => '已取消',
        Enums\SubscriptionStatus::SUSPENDED => '已暫停',
        Enums\SubscriptionStatus::EXPIRED   => '已逾期',
    ],
];
