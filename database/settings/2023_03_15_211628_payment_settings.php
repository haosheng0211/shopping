<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class PaymentSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('payment.hash_key');
        $this->migrator->add('payment.hash_iv');
        $this->migrator->add('payment.test_mode', true);
        $this->migrator->add('payment.merchant_id');
    }
}
