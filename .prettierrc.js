module.exports = {
    tabWidth: 4,
    printWidth: 180,
    overrides: [
        {
            files: ["*.blade.php"],
            options: {
                parser: "blade",
            },
        },
    ],
};
