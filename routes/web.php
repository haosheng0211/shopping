<?php

use App\Http\Controllers;

Route::any('/payment/{order}/notify', [Controllers\PaymentController::class, 'notify'])->name('payment.notify');
Route::any('/payment/{order}/return', [Controllers\PaymentController::class, 'return'])->name('payment.return');
Route::any('/payment/{order}/request', [Controllers\PaymentController::class, 'request'])->name('payment.request');
Route::get('/order/{order}', Controllers\OrderController::class)->name('order');
