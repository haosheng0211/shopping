import Swal from "sweetalert2";

window.addEventListener("swal:toast", (event) => {
    Swal.fire({
        icon: event.detail.icon,
        title: event.detail.title,
        toast: true,
        timer: 2000,
        position: "top-end",
        showConfirmButton: false,
    });
});
