function show(modal) {
    modal.classList.remove("hidden");
    modal.querySelector(".modal-content").classList.add("animate__animated", "animate__zoomIn");
}

function hide(modal) {
    modal.querySelector(".modal-content").classList.add("animate__zoomOut");

    setTimeout(() => {
        modal.classList.add("hidden");
        modal.querySelector(".modal-content").classList.remove("animate__animated", "animate__zoomOut");
    }, 500);
}

document.addEventListener("show:product-modal", function () {
    const modal = document.getElementById("product-modal");

    if (!modal) {
        return;
    }

    show(modal);

    modal.querySelector(".modal-close").addEventListener("click", () => {
        hide(modal);
    });
});

document.addEventListener("hide:product-modal", function () {
    const modal = document.getElementById("product-modal");

    if (!modal) {
        return;
    }

    hide(modal);
});
