function changeColor(link, color, backgroundColor) {
    link.querySelector("a").style.color = color;
    link.style.backgroundColor = backgroundColor;
}

document.addEventListener("DOMContentLoaded", () => {
    const links = document.querySelectorAll("#navbar ul li");

    links.forEach((link) => {
        const color = link.dataset.color;

        link.addEventListener("mouseover", () => {
            changeColor(link, "#fff", color);
        });

        link.addEventListener("mouseout", () => {
            changeColor(link, color, "transparent");
        });
    });
});
