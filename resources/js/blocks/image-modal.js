function show(modal) {
    modal.classList.remove("hidden");
    modal.querySelector(".modal-content").classList.add("animate__animated", "animate__zoomIn");
}

function hide(modal) {
    modal.querySelector(".modal-content").classList.add("animate__zoomOut");

    setTimeout(() => {
        modal.classList.add("hidden");
        modal.querySelector(".modal-content").classList.remove("animate__animated", "animate__zoomOut");
    }, 500);
}

document.addEventListener("DOMContentLoaded", function () {
    const modal = document.getElementById("image-modal");

    if (!modal) {
        return;
    }

    show(modal);

    modal.querySelector(".modal-close").addEventListener("click", () => {
        hide(modal);
    });

    setTimeout(() => {
        hide(modal);
    }, 3000);
});
