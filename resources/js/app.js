import "./plugins/flowbite";
import "./plugins/sweetalert";
import "./blocks/navbar";
import "./blocks/image-modal";
import "./blocks/product-modal";

document.addEventListener("contextmenu", (event) => event.preventDefault());
