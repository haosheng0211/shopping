<div id="product-modal" class="fixed w-full h-full top-0 left-0 flex items-center justify-center hidden z-50" wire:ignore.self>
    <div class="absolute w-full h-full bg-gray-900 opacity-50"></div>
    <div class="modal-content bg-white w-full max-w-[300px] mx-auto rounded-lg shadow-lg overflow-y-auto animate__animated" wire:ignore.self>
        @if ($product)
            <div class="text-left relative">
                <button type="button"
                    class="modal-close absolute top-0 right-0 text-gray-100 bg-transparent hover:text-gray-300 rounded-lg text-sm p-3 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
                <img class="product-image w-full" src="{{ Storage::url($product->image) }}" alt="" style="height: 300px;" />
                <div class="p-3">
                    <h2 class="product-name text-2xl font-bold mb-4">{{ $product->name }}</h2>
                    <p class="product-description text-gray-600 mb-4">{{ $product->description }}</p>
                    <div class="flex justify-between items-center">
                        <div class="flex flex-row h-8 h-10 w-28 rounded-lg">
                            <button class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:bg-gray-200 h-full w-36 rounded-l cursor-pointer outline-none"
                                wire:click="decrementProduct">
                                <span class="m-auto text-lg font-thin">−</span>
                            </button>
                            <input type="number"
                                class="outline-none focus:outline-none text-center bg-gray-100 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none border-0"
                                name="custom-input-number" value="{{ $quantity }}" readonly style="width: 100%;">
                            <button class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:bg-gray-200 h-full w-36 rounded-r cursor-pointer" wire:click="incrementProduct">
                                <span class="m-auto text-lg font-thin">+</span>
                            </button>
                        </div>
                        <div class="flex items-center space-x-1.5 rounded-full bg-blue-500 px-4 py-1.5 text-white duration-100 hover:bg-blue-600">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-4 w-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                            </svg>
                            <button type="button" class="text-sm" wire:click="addToCart">加入認購</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
