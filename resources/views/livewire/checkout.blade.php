<section id="checkout" class="py-8">
    <h1 class="text-center text-2xl text-gray-800 font-bold">
        填寫資料
    </h1>
    <form wire:submit.prevent="submit">
        <div class="p-6 my-8 bg-white border border-gray-200 rounded shadow dark:bg-gray-800 dark:border-gray-700">
            <div class="mb-6">
                <label for="consumer_name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">姓名（必填）
                    <span style="color: red">*</span>
                </label>
                <input type="text" id="consumer_name" wire:model="consumer_name"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="" required>
                @error('consumer_name')
                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label for="consumer_phone" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">電話（必填）
                    <span style="color: red">*</span>
                </label>
                <input type="text" id="consumer_phone" wire:model="consumer_phone"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="" required>
                @error('consumer_phone')
                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label for="consumer_email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">信箱</label>
                <input type="text" id="consumer_email" wire:model="consumer_email"
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="">
                @error('consumer_email')
                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label for="type" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">付款方式</label>
                <div class="flex">
                    <div class="flex items-center mr-4">
                        <input id="order-type-one-time" wire:model="order_type" type="radio" value="{{ OrderType::ONE_TIME }}" name="inline-radio-group"
                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="order-type-one-time" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">一次付清</label>
                    </div>
                    <div class="flex items-center mr-4">
                        <input id="order-type-subscription" wire:model="order_type" type="radio" value="{{ OrderType::SUBSCRIPTION }}" name="inline-radio-group"
                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                        <label for="order-type-subscription" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">定期定額</label>
                    </div>
                </div>
            </div>
            @if ($order_type === OrderType::SUBSCRIPTION)
                <div class="mb-6">
                    <select id="subscription_duration" wire:model="subscription_duration"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option value="3">3個月</option>
                        <option value="6">6個月</option>
                        <option value="9">9個月</option>
                        <option value="12">12個月</option>
                    </select>
                </div>
            @endif
            <div class="mb-6">
                <label for="remark" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">備註</label>
                <textarea id="remark" wire:model="remark" rows="4"
                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded border border-gray-300 focus:ring-blue-500 focus:border-blue-500"></textarea>
                @error('remark')
                    <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p>
                @enderror
            </div>
        </div>

        <button type="submit"
            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 w-full">
            立即結帳
        </button>
    </form>
</section>
