<section id="cart" class="py-8">
    <h1 class="text-center text-2xl text-gray-800 font-bold">
        目前認購
    </h1>
    <div class="mt-8 bg-white border border-gray-200 rounded shadow dark:bg-gray-800 dark:border-gray-700">
        <div class="flex-1 overflow-y-auto sm:px-6">
            @if (!count($cart->products))
                <div class="text-center p-12">
                    購物車「沒有產品」，請先將產品「加入購物車」
                </div>
            @else
                <ul role="list" class="divide-y divide-gray-200 px-4">
                    @foreach ($cart->products as $index => $product)
                        <li class="flex py-6" wire:key="{{ $product->id }}">
                            <div class="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                                <img src="{{ Storage::url($product->image) }}" alt="{{ $product->name }}" class="h-full w-full object-cover object-center">
                            </div>

                            <div class="ml-4 flex flex-1 flex-col">
                                <div>
                                    <div class="flex justify-between text-base font-medium text-gray-900">
                                        <h3>
                                            <a href="#">{{ $product->name }}</a>
                                        </h3>
                                        <p class="ml-4">
                                            ${{ number_format($product->pivot->total_amount) }}
                                        </p>
                                    </div>
                                </div>
                                <div class="flex flex-1 items-end justify-between text-sm">
                                    <div class="flex flex-row h-8 h-10 w-24 rounded-lg">
                                        <button class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:bg-gray-200 h-full w-36 rounded-l cursor-pointer outline-none"
                                            wire:click="decrementProduct({{ $product->id }})">
                                            <span class="m-auto text-lg font-thin">−</span>
                                        </button>
                                        <input type="number"
                                            class="outline-none focus:outline-none text-center w-full bg-gray-100 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none border-0"
                                            name="custom-input-number" value="{{ $product->pivot->quantity }}">
                                        <button class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:bg-gray-200 h-full w-36 rounded-r cursor-pointer"
                                            wire:click="incrementProduct({{ $product->id }})">
                                            <span class="m-auto text-lg font-thin">+</span>
                                        </button>
                                    </div>
                                    <div class="flex">
                                        <button type="button" class="font-medium text-indigo-600 hover:text-indigo-500" wire:click="removeProduct({{ $product->id }})">
                                            移除
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>

        <div class="border-t border-gray-200 py-6 px-4 sm:px-6">
            <div class="flex justify-between text-base font-medium text-gray-900 items-center">
                <p>小計</p>
                <p class="text-2xl">${{ number_format($cart->total_amount) }}</p>
            </div>
        </div>
    </div>
</section>
