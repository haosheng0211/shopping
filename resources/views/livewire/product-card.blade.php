<article class="p-4 bg-white border border-gray-200 rounded shadow dark:bg-gray-800 dark:border-gray-700">
    <img src="{{ Storage::url($product->image) }}" class="w-full h-[180px] rounded" alt="{{ $product->name }}">
    <h2 class="text-lg mt-3">{{ $product->name }}</h2>
    <h3 class="text-sm text-gray-400 mt-1 line-clamp-3">{{ $product->description }}</h3>
    <div class="mt-3 flex justify-between">
        <p class="block text-lg font-bold text-blue-500">${{ number_format($product->price) }}</p>
        <div class="flex items-center space-x-1.5 rounded-full bg-blue-500 px-4 py-1.5 text-white duration-100 hover:bg-blue-600">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-4 w-4">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
            </svg>
            <button type="button" class="text-sm" wire:click="showProductModal">加入認購</button>
        </div>
    </div>
</article>
