@aware(['page'])
@props(['logo', 'items' => []])

<header class="fixed top-0 left-0 right-0">
    <nav class="bg-white border-gray-200 px-2 sm:px-4 py-2.5 shadow">
        <div class="max-w-6xl flex flex-wrap items-center justify-between mx-auto">
            <a href="" class="pl-2 md:pl-0">
                <img src="{{ Storage::url($logo) }}" class="h-6 mr-3 sm:h-9" alt="" />
            </a>
            <button data-collapse-toggle="navbar" type="button"
                class="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200"
                aria-controls="navbar" aria-expanded="false">
                <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                        clip-rule="evenodd"></path>
                </svg>
            </button>
            <div class="hidden w-full md:block md:w-auto" id="navbar">
                <ul class="flex flex-col mt-4 md:flex-row md:space-x-4 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white">
                    @foreach ($items as $item)
                        @if ($item['type'] === 'drawer')
                            <li class="navbar-item border rounded-full p-1 pr-2.5 mb-4 md:mb-0 last:mb-0" style="border-color: {{ $item['color'] }}"
                                data-color="{{ $item['color'] }}" data-drawer-target="drawer-backdrop">
                                <a href="javascript:void(0)" class="flex items-center py-2 pl-3 pr-4 rounded md:p-0" style="color: {{ $item['color'] }}"
                                    data-drawer-target="{{ $item['drawer'] }}" data-drawer-show="{{ $item['drawer'] }}" data-drawer-placement="right">
                                    <img src="{{ Storage::url($item['icon']) }}" alt="" class="inline-block mr-2 shadow rounded-full" width="30" height="30">
                                    {{ $item['text'] }}
                                </a>
                            </li>
                        @else
                            <li class="navbar-item border rounded-full p-1 pr-2.5 mb-4 md:mb-0 last:mb-0" style="border-color: {{ $item['color'] }}"
                                data-color="{{ $item['color'] }}">
                                <a href="{{ $item['href'] }}" target="{{ $item['target'] }}" class="flex items-center py-2 pl-3 pr-4 rounded md:p-0"
                                    style="color: {{ $item['color'] }}">
                                    <img src="{{ Storage::url($item['icon']) }}" alt="" class="inline-block mr-2 shadow rounded-full" width="30" height="30">
                                    {{ $item['text'] }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
</header>

@foreach ($items as $item)
    @if ($item['type'] === 'drawer')
        <div id="{{ $item['drawer'] }}" class="fixed top-0 right-0 z-40 h-screen p-4 overflow-y-auto transition-transform translate-x-full bg-white max-w-2xl dark:bg-gray-800"
            tabindex="-1" aria-labelledby="drawer-right-label">
            <h5 id="drawer-right-label" class="inline-flex items-center mb-4 text-base font-semibold text-gray-500 dark:text-gray-400">
                <svg class="w-5 h-5 mr-2" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                        clip-rule="evenodd"></path>
                </svg>
                {{ $item['text'] }}
            </h5>
            <button type="button" data-drawer-hide="{{ $item['drawer'] }}" aria-controls="{{ $item['drawer'] }}"
                class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 right-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clip-rule="evenodd"></path>
                </svg>
                <span class="sr-only">Close menu</span>
            </button>
            {!! $item['content'] !!}
        </div>
    @endif
@endforeach
