@aware(['page'])
@props(['height', 'youtube_id'])
<div class="video-block">
    <iframe style="width: 100%; height: {{ $height }}vh"
        src="https://www.youtube.com/embed/{{ $youtube_id }}?autoplay=1&mute=1&controls=0&loop=1&playlist={{ $youtube_id }}" title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
