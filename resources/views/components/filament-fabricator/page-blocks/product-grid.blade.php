@aware(['page'])
@props([
    'title',
    'anchor' => '',
    'columns' => [
        'lg' => 3,
        'md' => 2,
        'sm' => 1,
    ],
    'products' => [],
])

<section id="{{ $anchor }}" class="py-8">
    <h1 class="text-center text-2xl text-gray-800 font-bold">
        {{ $title }}
    </h1>
    <div class="mt-8 grid grid-cols-{{ $columns['sm'] }} md:grid-cols-{{ $columns['md'] }} lg:grid-cols-{{ $columns['lg'] }} gap-4">
        @foreach ($products as $id)
            <livewire:product-card :product-id="$id" />
        @endforeach
    </div>
    <livewire:product-modal></livewire:product-modal>
</section>
