@aware(['page'])
@props(['content'])

<section>
    {!! $content !!}
</section>
