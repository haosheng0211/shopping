@aware(['page'])
@props(['image'])

<div id="image-modal" class="fixed w-full h-full top-0 left-0 flex items-center justify-center hidden z-50">
    <div class="absolute w-full h-full bg-gray-900 opacity-50"></div>
    <div class="modal-content bg-white max-w-[300px] md:max-w-2xl mx-auto rounded-lg shadow-lg overflow-y-auto animate__animated">
        <div class="text-left relative">
            <button type="button"
                class="modal-close absolute top-0 right-0 text-gray-100 bg-transparent hover:text-gray-300 rounded-lg text-sm p-3 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clip-rule="evenodd"></path>
                </svg>
                <span class="sr-only">Close modal</span>
            </button>
            <img src="{{ Storage::url($image) }}" alt="" />
        </div>
    </div>
</div>
