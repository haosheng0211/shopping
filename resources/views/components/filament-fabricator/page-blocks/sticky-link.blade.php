@aware(['page'])
@props(['items' => []])

@if (count($items))
    <div class="fixed bottom-10 right-5 z-20">
        @foreach ($items as $item)
            <a href="{{ $item['href'] }}" target="{{ $item['target'] }}" class="block mt-5">
                <img src="{{ Storage::url($item['icon']) }}" alt="" width="55" height="55" class="shadow-lg rounded-full">
                @if (isset($item['text']))
                    <span class="-mt-3 block text-white text-sm"
                        style="text-shadow: -1px 0 {{ $item['color'] }}, 0 1px {{ $item['color'] }}, 1px 0 {{ $item['color'] }}, 0 -1px {{ $item['color'] }};">
                        {{ $item['text'] }}
                    </span>
                @endif
            </a>
        @endforeach
    </div>
@endif
