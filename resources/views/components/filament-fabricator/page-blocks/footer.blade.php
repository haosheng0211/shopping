@aware(['page'])
@props(['company_name'])

<footer class="text-gray-600 body-font">
    <div class="max-w-6xl py-6 mx-auto flex items-center justify-center">
        <p class="text-sm text-gray-500 sm:ml-6 sm:mt-0 mt-4">© 2020 {{ $company_name }}</p>
    </div>
</footer>