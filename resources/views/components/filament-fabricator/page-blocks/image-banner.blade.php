@aware(['page'])
@props(['image'])
<section>
    <img class="block w-full" src="{{ Storage::url($image) }}" alt="">
</section>
