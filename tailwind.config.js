module.exports = {
    content: ["./resources/**/*.blade.php", "./resources/**/*.js", "./node_modules/flowbite/**/*.js"],
    plugins: [require("flowbite/plugin")],
    theme: {
        extend: {},
    },
    safelist: [
        {
            pattern: /grid-cols-\d+$/,
            variants: ["sm", "md", "lg", "xl", "2xl"],
        },
    ],
};
