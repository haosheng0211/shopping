module.exports = {
    root: true,
    env: {
        node: true,
        browser: true,
    },
    extends: ["eslint:recommended", "plugin:prettier/recommended"],
    parserOptions: {
        sourceType: "module",
        ecmaVersion: "latest",
    },
    rules: {
        "padding-line-between-statements": [
            "error",
            {
                blankLine: "always",
                next: "*",
                prev: ["import"],
            },
            {
                blankLine: "never",
                next: ["import"],
                prev: ["import"],
            },
            {
                blankLine: "always",
                next: "*",
                prev: ["export"],
            },
            {
                blankLine: "always",
                next: "*",
                prev: ["function", "if", "switch", "try"],
            },
            {
                blankLine: "always",
                next: ["function", "if", "switch", "try"],
                prev: "*",
            },
            {
                blankLine: "always",
                next: ["return"],
                prev: "*",
            },
            {
                blankLine: "always",
                next: "*",
                prev: ["const", "let", "var"],
            },
            {
                blankLine: "never",
                next: ["const", "let", "var"],
                prev: ["const", "let", "var"],
            },
        ],
        "sort-imports": [
            "error",
            {
                memberSyntaxSortOrder: ["single", "all", "multiple", "none"],
            },
        ],
    },
};
