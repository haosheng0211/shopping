<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductResource\Pages;
use App\Models\Product;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class ProductResource extends Resource
{
    protected static ?string $label = '商品';

    protected static ?string $model = Product::class;

    public static function form(Form $form): Form
    {
        return $form->schema([
            Forms\Components\Card::make([
                Forms\Components\TextInput::make('name')->label(trans('validation.attributes.name'))->required(),
                Forms\Components\FileUpload::make('image')->label(trans('validation.attributes.image'))->required(),
                Forms\Components\Textarea::make('description')->label(trans('validation.attributes.description'))->required(),
                Forms\Components\TextInput::make('price')->label(trans('validation.attributes.price'))->required()->integer(),
            ]),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            Tables\Columns\TextColumn::make('name')->label(trans('validation.attributes.name'))->searchable()->sortable(),
            Tables\Columns\ImageColumn::make('image')->label(trans('validation.attributes.image')),
            Tables\Columns\TextColumn::make('price')->label(trans('validation.attributes.price')),
            Tables\Columns\TextColumn::make('created_at')->label(trans('validation.attributes.created_at'))->dateTime(),
        ]);
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListProducts::route('/'),
            'create' => Pages\CreateProduct::route('/create'),
            'edit'   => Pages\EditProduct::route('/{record}/edit'),
        ];
    }
}
