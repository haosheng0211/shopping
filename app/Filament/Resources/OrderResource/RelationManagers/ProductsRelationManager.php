<?php

namespace App\Filament\Resources\OrderResource\RelationManagers;

use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;

class ProductsRelationManager extends RelationManager
{
    protected static ?string $label = '選購產品';

    protected static string $relationship = 'products';

    public static function table(Table $table): Table
    {
        return $table->columns([
            Tables\Columns\TextColumn::make('name')->label(trans('validation.attributes.name')),
            Tables\Columns\TextColumn::make('price')->label(trans('validation.attributes.price'))->money('TWD'),
            Tables\Columns\TextColumn::make('pivot.quantity')->label(trans('validation.attributes.quantity')),
            Tables\Columns\TextColumn::make('pivot.total_amount')->label(trans('validation.attributes.total_amount'))->money('TWD'),
        ]);
    }
}
