<?php

namespace App\Filament\Resources\OrderResource\RelationManagers;

use App\Enums\OrderType;
use App\Enums\PaymentMethod;
use App\Enums\PaymentStatus;
use App\Models\Order;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPaymentsRelationManager extends RelationManager
{
    protected static ?string $label = '付款紀錄';

    protected static string $relationship = 'subscriptionPayments';

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('method')->label(trans('validation.attributes.payment_method'))->enum(PaymentMethod::asSelectArray()),
            TextColumn::make('amount')->label(trans('validation.attributes.payment_amount'))->money('TWD'),
            TextColumn::make('status')->label(trans('validation.attributes.payment_status'))->enum(PaymentStatus::asSelectArray()),
            TextColumn::make('created_at')->label(trans('validation.attributes.date')),
        ]);
    }

    public static function canViewForRecord(Model|Order $ownerRecord): bool
    {
        return $ownerRecord->type === OrderType::SUBSCRIPTION;
    }
}
