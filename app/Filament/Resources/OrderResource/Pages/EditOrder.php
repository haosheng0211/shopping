<?php

namespace App\Filament\Resources\OrderResource\Pages;

use App\Filament\Resources\OrderResource;
use Filament\Resources\Pages\EditRecord;

class EditOrder extends EditRecord
{
    protected static string $view = 'filament.resources.order-resource.pages.edit-order';

    protected static string $resource = OrderResource::class;

    protected function getActions(): array
    {
        return [];
    }
}
