<?php

namespace App\Filament\Resources;

class PageResource extends \Z3d0X\FilamentFabricator\Resources\PageResource
{
    protected static ?string $label = '頁面';

    protected static ?string $navigationGroup = '設定';
}
