<?php

namespace App\Filament\Resources;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Filament\Resources\OrderResource\Pages;
use App\Filament\Resources\OrderResource\RelationManagers;
use App\Models\Order;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;

class OrderResource extends Resource
{
    protected static ?string $label = '訂單';

    protected static ?string $model = Order::class;

    protected static ?string $navigationIcon = 'heroicon-o-shopping-cart';

    public static function form(Form $form): Form
    {
        return $form->schema([
            Forms\Components\Section::make('訂單資訊')->schema(function (?Order $record) {
                $forms = [
                    Forms\Components\Placeholder::make('id')->label(trans('validation.attributes.id'))->content($record->id),
                    Forms\Components\Placeholder::make('type')->label(trans('validation.attributes.order_type'))->content(OrderType::getDescription($record->type)),
                    Forms\Components\Placeholder::make('total_amount')->label(trans('validation.attributes.total_amount'))->content(number_format($record->total_amount)),
                ];

                if (! is_null($subscription = $record->subscription)) {
                    $forms[] = Forms\Components\Placeholder::make('frequency')->label(trans('validation.attributes.subscription_duration'))->content($subscription->duration . ' 個月');
                    $forms[] = Forms\Components\Placeholder::make('date')->label(trans('validation.attributes.subscription_date'))->content($subscription->start_date . ' ~ ' . $subscription->end_date);
                }

                $forms[] = Forms\Components\Select::make('status')->label(trans('validation.attributes.order_status'))->options(OrderStatus::asSelectArray());

                return $forms;
            }),
            Forms\Components\Section::make('消費者資料')->schema(function (?Order $record) {
                $consumer = $record->consumer;

                return [
                    Forms\Components\Placeholder::make('name')->label(trans('validation.attributes.consumer_name'))->content($consumer->name),
                    Forms\Components\Placeholder::make('phone')->label(trans('validation.attributes.consumer_phone'))->content($consumer->phone),
                    Forms\Components\Placeholder::make('email')->label(trans('validation.attributes.consumer_email'))->content($consumer->email),
                    Forms\Components\Placeholder::make('remark')->label(trans('validation.attributes.remark'))->content($consumer->remark),
                ];
            }),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            Tables\Columns\TextColumn::make('id')->label(trans('validation.attributes.id'))->searchable()->sortable(),
            Tables\Columns\BadgeColumn::make('type')->label(trans('validation.attributes.order_type'))->sortable()->enum(OrderType::asSelectArray()),
            Tables\Columns\TextColumn::make('consumer.name')->label(trans('validation.attributes.consumer_name'))->searchable()->sortable(),
            Tables\Columns\TextColumn::make('consumer.phone')->label(trans('validation.attributes.consumer_phone'))->searchable()->sortable(),
            Tables\Columns\TextColumn::make('consumer.email')->label(trans('validation.attributes.consumer_email'))->searchable()->sortable(),
            Tables\Columns\TextColumn::make('total_amount')->label(trans('validation.attributes.total_amount'))->sortable()->money('TWD'),
            Tables\Columns\BadgeColumn::make('status')->label(trans('validation.attributes.order_status'))->sortable()->enum(OrderStatus::asSelectArray()),
            Tables\Columns\TextColumn::make('created_at')->label(trans('validation.attributes.created_at'))->sortable(),
        ])->filters([
            Tables\Filters\SelectFilter::make('type')->label(trans('validation.attributes.order_type'))->options(OrderType::asSelectArray())->multiple(),
            Tables\Filters\SelectFilter::make('status')->label(trans('validation.attributes.order_status'))->options(OrderStatus::asSelectArray())->multiple(),
        ])->defaultSort('created_at', 'desc');
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\ProductsRelationManager::class,
            RelationManagers\SubscriptionPaymentsRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOrders::route('/'),
            'edit'  => Pages\EditOrder::route('/{record}'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->with(['consumer', 'products', 'subscription']);
    }

    protected static function getNavigationBadge(): ?string
    {
        return Order::where('status', OrderStatus::PROCESSING)->count();
    }

    protected static function getNavigationBadgeColor(): ?string
    {
        return 'danger';
    }
}
