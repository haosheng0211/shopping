<?php

namespace App\Filament\Fabricator\Layouts;

use Z3d0X\FilamentFabricator\Layouts\Layout;

class ShoppingLayout extends Layout
{
    protected static ?string $name = 'shopping';

    public static function getLabel(): string
    {
        return '一頁式購物';
    }
}
