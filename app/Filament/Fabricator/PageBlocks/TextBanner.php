<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class TextBanner extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('text-banner')->schema([
            TinyEditor::make('content')->label('文字')->required(),
        ])->label('文字橫幅');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
