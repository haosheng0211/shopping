<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class ImageModal extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('image-modal')->schema([
            Forms\Components\FileUpload::make('image')->label(trans('validation.attributes.image'))->required(),
        ])->label('圖片彈出視窗');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
