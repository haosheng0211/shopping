<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class Footer extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('footer')->schema([
            Forms\Components\TextInput::make('company_name')->label('公司名稱'),
        ])->label('底部資訊');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
