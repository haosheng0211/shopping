<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class Cart extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('cart')->schema([
        ])->label('購物車');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
