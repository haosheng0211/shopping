<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Illuminate\Support\Str;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class StickyLink extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('sticky-link')->schema([
            Forms\Components\Repeater::make('items')->schema([
                Forms\Components\FileUpload::make('icon')->label(trans('validation.attributes.icon'))->required()->hint('建議尺寸: 64x64'),
                Forms\Components\TextInput::make('text')->label(trans('validation.attributes.text'))->required(),
                Forms\Components\ColorPicker::make('color')->label(trans('validation.attributes.color'))->required(),
                Forms\Components\Select::make('type')->label(trans('validation.attributes.type'))->options([
                    'url'    => '連結',
                    'tel'    => '電話',
                    'mailto' => '信箱',
                ])->required(),
                Forms\Components\TextInput::make('href')->label(trans('validation.attributes.href'))->required(),
            ])->label('連結'),
        ])->label('懸浮連結');
    }

    public static function mutateData(array $data): array
    {
        foreach ($data['items'] as $key => $item) {
            switch ($item['type']) {
                case 'url':
                    if (Str::startsWith($item['href'], 'http')) {
                        $data['items'][$key]['target'] = '_blank';
                    } else {
                        $data['items'][$key]['target'] = '_self';
                    }
                    break;
                case 'tel':
                case 'mailto':
                    $data['items'][$key]['href'] = $item['type'] . ':' . $item['href'];
                    $data['items'][$key]['target'] = '_self';
                    break;
            }
        }

        return $data;
    }
}
