<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Illuminate\Support\Str;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class Navbar extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('navbar')->schema([
            Forms\Components\FileUpload::make('logo')->label('Logo')->required(),
            Forms\Components\Repeater::make('items')->schema([
                Forms\Components\FileUpload::make('icon')->label(trans('validation.attributes.icon'))->required()->hint('建議尺寸: 32x32'),
                Forms\Components\TextInput::make('text')->label(trans('validation.attributes.text'))->required(),
                Forms\Components\ColorPicker::make('color')->label(trans('validation.attributes.color'))->required(),
                Forms\Components\Select::make('type')->label(trans('validation.attributes.type'))->options([
                    'url'    => '連結',
                    'tel'    => '電話',
                    'drawer' => '文本抽屜',
                    'mailto' => '信箱',
                ])->required()->reactive(),
                Forms\Components\TextInput::make('href')->label(trans('validation.attributes.href'))->required()->hidden(fn ($get) => $get('type') === 'drawer'),
                TinyEditor::make('content')->label('內容')->required()->hidden(fn ($get) => $get('type') !== 'drawer'),
            ])->label('連結'),
        ])->label('頂部導覽列');
    }

    public static function mutateData(array $data): array
    {
        foreach ($data['items'] as $key => $item) {
            switch ($item['type']) {
                case 'url':
                    if (Str::startsWith($item['href'], 'http')) {
                        $data['items'][$key]['target'] = '_blank';
                    } else {
                        $data['items'][$key]['target'] = '_self';
                    }
                    break;
                case 'tel':
                case 'mailto':
                    $data['items'][$key]['href'] = $item['type'] . ':' . $item['href'];
                    $data['items'][$key]['target'] = '_self';
                    break;
                case 'drawer':
                    $data['items'][$key]['href'] = '#';
                    $data['items'][$key]['target'] = '_self';
                    $data['items'][$key]['drawer'] = 'drawer-' . $key;
                    break;
            }
        }

        return $data;
    }
}
