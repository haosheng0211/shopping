<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class ImageBanner extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('image-banner')->schema([
            Forms\Components\FileUpload::make('image')->label('圖片')->required(),
        ])->label('圖片橫幅');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
