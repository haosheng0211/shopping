<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Filament\Forms\Components\Builder\Block;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class Youtube extends PageBlock
{
    public static function getBlockSchema(): Block
    {
        return Block::make('youtube')->schema([
            Forms\Components\TextInput::make('url')->label(trans('validation.attributes.youtube_url'))->placeholder('https://www.youtube.com/watch?v=')->required(),
            Forms\Components\TextInput::make('height')->label(trans('validation.attributes.height'))->suffix('%')->required()->default(100)->integer()->minValue(1)->maxValue(100),
        ]);
    }

    public static function mutateData(array $data): array
    {
        if (isset($data['url'])) {
            $parse_url = parse_url($data['url'], PHP_URL_QUERY);
            parse_str($parse_url, $parse_str);
            $data['youtube_id'] = $parse_str['v'] ?? null;
        }

        return $data;
    }
}
