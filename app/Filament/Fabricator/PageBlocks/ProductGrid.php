<?php

namespace App\Filament\Fabricator\PageBlocks;

use App\Models\Product;
use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class ProductGrid extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('product-grid')->schema([
            Forms\Components\TextInput::make('anchor')->label(trans('validation.attributes.anchor')),
            Forms\Components\TextInput::make('title')->label(trans('validation.attributes.title'))->required(),
            Forms\Components\Select::make('products')->options(function () {
                return Product::all()->pluck('name', 'id');
            })->multiple()->label(trans('validation.attributes.product')),
            Forms\Components\Fieldset::make(trans('validation.attributes.columns'))->schema([
                Forms\Components\Select::make('columns.lg')->options([
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                ])->default(3)->label('電腦版'),
                Forms\Components\Select::make('columns.md')->options([
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                ])->default(2)->label('平板版'),
                Forms\Components\Select::make('columns.sm')->options([
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                ])->default(1)->label('手機版'),
            ])->columns(1),
        ])->label('商品列表(格子)');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
