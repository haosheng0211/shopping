<?php

namespace App\Filament\Fabricator\PageBlocks;

use Filament\Forms;
use Z3d0X\FilamentFabricator\PageBlocks\PageBlock;

class Checkout extends PageBlock
{
    public static function getBlockSchema(): Forms\Components\Builder\Block
    {
        return Forms\Components\Builder\Block::make('checkout')->schema([
        ])->label('結帳表單');
    }

    public static function mutateData(array $data): array
    {
        return $data;
    }
}
