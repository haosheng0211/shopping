<?php

namespace App\Filament\Widgets\Sales;

use App\Enums\OrderStatus;
use App\Models\Order;
use Filament\Widgets\BarChartWidget;
use Illuminate\Support\Carbon;

class SalesWeekTrend extends BarChartWidget
{
    protected static ?string $heading = '最近一週銷售趨勢';

    protected static ?string $pollingInterval = '60s';

    protected static ?array $options = [
        'plugins' => [
            'legend' => [
                'display' => false,
            ],
        ],
    ];

    protected function getData(): array
    {
        for ($i = 0; $i < 7; ++$i) {
            $carbon = Carbon::now()->subDays(6 - $i);
            $labels[] = $carbon->format('m/d');
            $totals[] = Order::whereDay('created_at', $carbon)->whereNot('status', OrderStatus::PENDING)->get()->sum('total_amount');
        }

        return [
            'labels'   => $labels,
            'datasets' => [
                [
                    'data'            => $totals,
                    'backgroundColor' => 'rgb(255, 99, 132)',
                ],
            ],
        ];
    }
}
