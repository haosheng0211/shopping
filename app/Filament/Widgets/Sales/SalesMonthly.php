<?php

namespace App\Filament\Widgets\Sales;

use App\Enums\OrderStatus;
use App\Models\Order;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;
use Illuminate\Support\Carbon;

class SalesMonthly extends BaseWidget
{
    protected function getCards(): array
    {
        $carbon = Carbon::now();
        $day_orders = Order::whereDay('created_at', $carbon)->whereNot('status', OrderStatus::PENDING)->get();
        $month_orders = Order::whereMonth('created_at', $carbon)->whereNot('status', OrderStatus::PENDING)->get();

        return [
            Card::make('本日銷售', number_format($day_orders->sum('total_amount'))),
            Card::make('本日訂單', number_format($day_orders->count())),
            Card::make('本月銷售', number_format($month_orders->sum('total_amount'))),
            Card::make('本月訂單', number_format($month_orders->count())),
        ];
    }
}
