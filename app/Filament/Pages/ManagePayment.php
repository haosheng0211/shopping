<?php

namespace App\Filament\Pages;

use App\Settings\PaymentSettings;
use Filament\Forms;
use Filament\Pages\SettingsPage;

class ManagePayment extends SettingsPage
{
    protected ?string $heading = '金流';

    protected static string $settings = PaymentSettings::class;

    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static ?string $navigationGroup = '設定';

    protected static ?string $navigationLabel = '金流';

    protected function getFormSchema(): array
    {
        return [
            Forms\Components\Card::make([
                Forms\Components\TextInput::make('merchant_id')->label(trans('validation.attributes.merchant_id')),
                Forms\Components\TextInput::make('hash_key')->label('Hash Key'),
                Forms\Components\TextInput::make('hash_iv')->label('Hash IV'),
                Forms\Components\Toggle::make('test_mode')->label(trans('validation.attributes.test_mode')),
            ]),
        ];
    }
}
