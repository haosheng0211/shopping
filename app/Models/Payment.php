<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $casts = [
        'status' => 'int',
    ];

    protected $fillable = ['method', 'amount', 'status'];
}
