<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Snowflake\SnowflakeCast;
use Snowflake\Snowflakes;

class Order extends Model
{
    use Snowflakes;

    protected $casts = [
        'id'     => SnowflakeCast::class,
        'type'   => 'int',
        'status' => 'int',
    ];

    protected $fillable = [
        'consumer_id',
        'type',
        'status',
    ];

    public function consumer(): BelongsTo
    {
        return $this->belongsTo(Consumer::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)->using(OrderProduct::class)->withPivot([
            'price',
            'quantity',
        ]);
    }

    public function payment(): MorphOne
    {
        return $this->morphOne(Payment::class, 'payable');
    }

    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class);
    }

    public function subscriptionPayments(): HasManyThrough
    {
        return $this->hasManyThrough(Payment::class, Subscription::class, 'order_id', 'payable_id')->where('payable_type', Subscription::class);
    }

    public function totalAmount(): Attribute
    {
        return Attribute::get(fn () => $this->products->sum('pivot.total_amount'));
    }
}
