<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Cart extends Model
{
    protected $fillable = ['session_id'];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)->using(CartProduct::class)->withPivot([
            'price',
            'quantity',
        ]);
    }

    public function totalAmount(): Attribute
    {
        return Attribute::get(fn () => $this->products->sum('pivot.total_amount'));
    }
}
