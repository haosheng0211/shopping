<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Snowflake\SnowflakeCast;

class Subscription extends Model
{
    protected $casts = [
        'order_id' => SnowflakeCast::class,
        'duration' => 'int',
        'status'   => 'int',
    ];

    protected $fillable = [
        'duration',
        'status',
    ];

    public function payments(): MorphMany
    {
        return $this->morphMany(Payment::class, 'payable');
    }

    public function startDate(): Attribute
    {
        return Attribute::get(fn () =>Carbon::parse($this->created_at)->startOfMonth()->format('Y-m'));
    }

    public function endDate(): Attribute
    {
        return Attribute::get(fn () => Carbon::parse($this->created_at)->addMonths($this->duration)->startOfMonth()->format('Y-m'));
    }
}
