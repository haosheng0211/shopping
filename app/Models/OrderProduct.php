<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Snowflake\SnowflakeCast;

class OrderProduct extends Pivot
{
    protected $casts = [
        'order_id' => SnowflakeCast::class,
    ];

    protected $fillable = [
        'price',
        'quantity',
    ];

    public function totalAmount(): Attribute
    {
        return Attribute::get(fn () => $this->price * $this->quantity);
    }
}
