<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CartProduct extends Pivot
{
    protected $fillable = [
        'price',
        'quantity',
    ];

    public function totalAmount(): Attribute
    {
        return Attribute::get(fn () => $this->price * $this->quantity);
    }
}
