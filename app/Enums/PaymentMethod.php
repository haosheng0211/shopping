<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class PaymentMethod extends Enum implements LocalizedEnum
{
    public const CREDIT = 'Credit';

    public const ATM = 'ATM';

    public const CVS = 'CVS';

    public const BARCODE = 'BARCODE';

    public const WEB_ATM = 'WebATM';
}
