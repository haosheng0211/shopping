<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class OrderStatus extends Enum implements LocalizedEnum
{
    public const PENDING = 1;

    public const PROCESSING = 2;

    public const SHIPPED = 3;

    public const DELIVERED = 4;

    public const CANCELLED = 5;
}
