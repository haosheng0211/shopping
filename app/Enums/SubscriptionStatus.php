<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class SubscriptionStatus extends Enum implements LocalizedEnum
{
    public const PENDING = 1;

    public const ACTIVE = 2;

    public const CANCELLED = 3;

    public const SUSPENDED = 4;

    public const EXPIRED = 5;
}
