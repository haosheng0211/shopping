<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class PaymentStatus extends Enum implements LocalizedEnum
{
    public const UNPAID = 1;

    public const PAID = 2;

    public const FAILED = 3;
}
