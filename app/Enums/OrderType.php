<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class OrderType extends Enum implements LocalizedEnum
{
    public const ONE_TIME = 1;

    public const SUBSCRIPTION = 2;
}
