<?php

namespace app\Settings;

use Spatie\LaravelSettings\Settings;

class PaymentSettings extends Settings
{
    public ?string $hash_iv;

    public ?string $hash_key;

    public ?bool $test_mode;

    public ?string $merchant_id;

    public static function group(): string
    {
        return 'payment';
    }

    public static function encrypted(): array
    {
        return ['hash_iv', 'hash_key', 'merchant_id'];
    }
}
