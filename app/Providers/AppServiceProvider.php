<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        $this->app['validator']->extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^09[0-9]{8}$/', $value);
        });
    }
}
