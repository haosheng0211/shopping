<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductCard extends Component
{
    public Product $product;

    public function mount($productId)
    {
        $this->product = Product::find($productId);
    }

    public function render(): View
    {
        return view('livewire.product-card');
    }

    public function showProductModal()
    {
        $this->emit('showProductModal', $this->product);
    }
}
