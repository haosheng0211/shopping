<?php

namespace App\Http\Livewire;

use App\Models\Cart as CartModel;
use App\Models\Product;
use App\Services\CartService;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Cart extends Component
{
    public CartModel $cart;

    protected $listeners = ['cartUpdated' => 'mount'];

    public function mount()
    {
        $this->cart = app(CartService::class)->getCart();
    }

    public function render(): View
    {
        return view('livewire.cart');
    }

    public function removeProduct($id)
    {
        $product = Product::find($id);

        if ($product) {
            app(CartService::class)->removeCartProduct($product);
        }

        $this->emit('cartUpdated');
    }

    public function incrementProduct($id)
    {
        $product = Product::find($id);

        if ($product) {
            app(CartService::class)->incrementCartProduct($product);
        }

        $this->emit('cartUpdated');
    }

    public function decrementProduct($id)
    {
        $product = Product::find($id);

        if ($product) {
            app(CartService::class)->decrementCartProduct($product);
        }

        $this->emit('cartUpdated');
    }
}
