<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Services\CartService;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class ProductModal extends Component
{
    public $quantity;

    public ?Product $product;

    protected $listeners = ['showProductModal'];

    public function mount()
    {
        $this->quantity = 1;
    }

    public function render(): View
    {
        return view('livewire.product-modal');
    }

    public function addToCart()
    {
        $result = app(CartService::class)->addCartProduct($this->product, $this->quantity);
        $this->hideProductModal();

        if (! $result) {
            $this->dispatchBrowserEvent('swal:toast', [
                'icon'  => 'error',
                'title' => '商品已經在購物車中',
            ]);

            return;
        }

        $this->emit('cartUpdated');

        $this->dispatchBrowserEvent('swal:toast', [
            'icon'  => 'success',
            'title' => '商品已經加入購物車',
        ]);
    }

    public function decrementProduct()
    {
        if ($this->quantity > 1) {
            --$this->quantity;
        }
    }

    public function incrementProduct()
    {
        ++$this->quantity;
    }

    public function showProductModal(Product $product)
    {
        $this->product = $product;
        $this->quantity = 1;
        $this->dispatchBrowserEvent('show:product-modal');
    }

    public function hideProductModal()
    {
        $this->product = null;
        $this->quantity = 1;
        $this->dispatchBrowserEvent('hide:product-modal');
    }
}
