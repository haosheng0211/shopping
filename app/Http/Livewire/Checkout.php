<?php

namespace App\Http\Livewire;

use App\Enums\OrderType;
use App\Models\Consumer;
use App\Services\CartService;
use App\Services\OrderService;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Checkout extends Component
{
    public $order_type;

    public $consumer_name;

    public $consumer_email;

    public $consumer_phone;

    public $subscription_duration;

    public $remark;

    public $rules = [
        'order_type'            => ['required'],
        'consumer_name'         => ['required', 'string', 'max:255'],
        'consumer_email'        => ['nullable', 'string', 'email', 'max:255'],
        'consumer_phone'        => ['required', 'string', 'phone', 'max:255'],
        'subscription_duration' => ['nullable', 'integer', 'min:1', 'max:12'],
        'remark'                => ['nullable', 'string', 'max:255'],
    ];

    public function mount()
    {
        $this->order_type = OrderType::SUBSCRIPTION;
        $this->subscription_duration = 3;
    }

    public function render(): View
    {
        return view('livewire.checkout');
    }

    public function updatedOrderType($value)
    {
        $this->order_type = (int) $value;

        if ($this->order_type === OrderType::SUBSCRIPTION) {
            $this->subscription_duration = 3;
        } else {
            $this->subscription_duration = null;
        }
    }

    public function submit()
    {
        $this->validate();

        try {
            DB::beginTransaction();
            $cart = app(CartService::class)->getCart();

            if (! $cart->products()->count()) {
                $this->dispatchBrowserEvent('swal:toast', [
                    'title' => '購物車內沒有商品，請先加入商品',
                    'icon'  => 'error',
                ]);

                return;
            }

            $consumer = Consumer::create([
                'name'   => $this->consumer_name,
                'email'  => $this->consumer_email,
                'phone'  => $this->consumer_phone,
                'remark' => $this->remark,
            ]);

            $order = match ($this->order_type) {
                OrderType::ONE_TIME     => app(OrderService::class)->createOneTimeOrder($consumer, $cart),
                OrderType::SUBSCRIPTION => app(OrderService::class)->createSubscriptionOrder($consumer, $cart, $this->subscription_duration),
            };

            DB::commit();

            return redirect()->route('payment.request', $order);
        } catch (\Throwable $e) {
            DB::rollBack();

            $this->dispatchBrowserEvent('swal:toast', [
                'title' => '訂單建立失敗，若無法下單，可立即聯絡線上客服',
                'icon'  => 'error',
            ]);

            Log::warning('訂單建立失敗: ' . $e->getMessage());
        }
    }
}
