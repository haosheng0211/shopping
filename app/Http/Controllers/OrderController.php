<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Models\Order;

class OrderController extends Controller
{
    public function __invoke(Order $order)
    {
        $order->load('products');

        if ($order->status !== OrderStatus::PENDING) {
            return view('order', compact('order'));
        }

        return redirect('/');
    }
}
