<?php

namespace App\Http\Controllers;

use App\Enums\OrderType;
use App\Models\Order;
use App\Services\Payment\Strategies\OneTimePaymentStrategy;
use App\Services\Payment\Strategies\SubscriptionPaymentStrategy;
use App\Services\PaymentService;

class PaymentController extends Controller
{
    public function __construct(protected PaymentService $paymentService)
    {
    }

    public function notify(PaymentService $paymentService, Order $order)
    {
        $strategy = match ($order->type) {
            OrderType::ONE_TIME     => OneTimePaymentStrategy::class,
            OrderType::SUBSCRIPTION => SubscriptionPaymentStrategy::class,
            default                 => throw new \Exception('Unknown order type'),
        };

        $paymentService->setPaymentStrategy(app($strategy))->handleCallback($order);
    }

    public function return(Order $order)
    {
        $strategy = match ($order->type) {
            OrderType::ONE_TIME     => OneTimePaymentStrategy::class,
            OrderType::SUBSCRIPTION => SubscriptionPaymentStrategy::class,
            default                 => throw new \Exception('Unknown order type'),
        };

        $this->paymentService->setPaymentStrategy(app($strategy))->handleCallback($order);

        return redirect()->route('order', $order);
    }

    public function request(Order $order)
    {
        if ($order->type === OrderType::SUBSCRIPTION) {
            return $this->paymentService->setPaymentStrategy(app(SubscriptionPaymentStrategy::class))->processPayment($order);
        }

        return $this->paymentService->setPaymentStrategy(app(OneTimePaymentStrategy::class))->processPayment($order);
    }
}
