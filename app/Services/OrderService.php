<?php

namespace App\Services;

use App\Enums\OrderStatus;
use App\Enums\OrderType;
use App\Enums\PaymentMethod;
use App\Enums\PaymentStatus;
use App\Enums\SubscriptionStatus;
use App\Exceptions\OrderException;
use App\Models\Cart;
use App\Models\Consumer;
use App\Models\Order;

class OrderService
{
    public function createOneTimeOrder(Consumer $consumer, Cart $cart): Order
    {
        return $this->createOrder($consumer, $cart, OrderType::ONE_TIME);
    }

    public function createSubscriptionOrder(Consumer $consumer, Cart $cart, int $duration): Order
    {
        $order = $this->createOrder($consumer, $cart, OrderType::SUBSCRIPTION, PaymentMethod::CREDIT);

        $order->subscription()->create([
            'duration' => $duration,
            'status'   => SubscriptionStatus::PENDING,
        ]);

        return $order;
    }

    private function createOrder(Consumer $consumer, Cart $cart, string $type, ?string $payment_method = null)
    {
        if (! $cart->products()->count()) {
            throw new OrderException('購物車內沒有商品');
        }

        $order = Order::create([
            'consumer_id'  => $consumer->id,
            'type'         => $type,
            'status'       => OrderStatus::PENDING,
            'total_amount' => $cart->total_amount,
        ]);

        foreach ($cart->products as $product) {
            $order->products()->attach($product->id, [
                'price'    => $product->price,
                'quantity' => $product->pivot->quantity,
            ]);
        }

        $order->payment()->create([
            'method' => $payment_method,
            'status' => PaymentStatus::UNPAID,
            'amount' => $cart->total_amount,
        ]);

        return $order;
    }
}
