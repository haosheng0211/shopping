<?php

namespace App\Services\Payment\Strategies;

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\Exceptions\PaymentException;
use App\Models\Order;
use App\Services\Payment\PaymentStrategy;
use Illuminate\Support\Str;
use Omnipay\Common\Message\RedirectResponseInterface;

class OneTimePaymentStrategy extends PaymentStrategy
{
    public function processPayment(Order $order)
    {
        $response = $this->gateway->purchase(array_merge($this->getInvoiceParams($order), $this->getPurchaseParams($order)))->send();

        if (! $response instanceof RedirectResponseInterface) {
            throw new PaymentException('金流服務發生異常');
        }

        return $response->redirect();
    }

    public function handleCallback(Order $order)
    {
        $response = $this->gateway->acceptNotification($this->request->all())->send();

        $order->update(['status' => OrderStatus::PROCESSING]);

        $method = Str::before($this->request->get('PaymentType'), '_');
        $status = $response->isSuccessful() ? PaymentStatus::PAID : PaymentStatus::FAILED;

        $order->payment->update([
            'method' => $method,
            'status' => $status,
        ]);
    }
}
