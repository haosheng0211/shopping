<?php

namespace App\Services\Payment\Strategies;

use App\Enums\OrderStatus;
use App\Enums\PaymentMethod;
use App\Enums\PaymentStatus;
use App\Exceptions\PaymentException;
use App\Models\Order;
use App\Services\Payment\PaymentStrategy;
use Omnipay\Common\Message\RedirectResponseInterface;

class SubscriptionPaymentStrategy extends PaymentStrategy
{
    public function processPayment(Order $order)
    {
        $response = $this->gateway->purchase(array_merge($this->getPeriodParams($order), $this->getInvoiceParams($order), $this->getPurchaseParams($order)))->send();

        if (! $response instanceof RedirectResponseInterface) {
            throw new PaymentException('金流服務發生異常');
        }

        return $response->redirect();
    }

    public function handleCallback(Order $order)
    {
        $response = $this->gateway->acceptNotification($this->request->all())->send();

        $order->update(['status' => OrderStatus::PROCESSING]);

        $method = PaymentMethod::CREDIT;
        $status = $response->isSuccessful() ? PaymentStatus::PAID : PaymentStatus::FAILED;

        $order->payment->update([
            'method' => $method,
            'status' => $status,
        ]);

        if ($order->subscription->payments()->whereMonth('created_at', now())->count() > 0) {
            return;
        }

        $order->subscription->payments()->create([
            'method' => $method,
            'status' => $status,
            'amount' => $order->payment->amount,
        ]);
    }
}
