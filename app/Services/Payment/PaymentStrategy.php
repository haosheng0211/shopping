<?php

namespace App\Services\Payment;

use App\Models\Order;
use App\Models\Product;
use app\Settings\PaymentSettings;
use Illuminate\Http\Request;
use Omnipay\Common\GatewayInterface;
use Omnipay\Omnipay;

abstract class PaymentStrategy
{
    protected GatewayInterface $gateway;

    public function __construct(protected Request $request, PaymentSettings $paymentSettings)
    {
        $this->gateway = Omnipay::create('ECPay');
        $this->gateway->initialize([
            'HashKey'     => $paymentSettings->hash_key,
            'HashIV'      => $paymentSettings->hash_iv,
            'testMode'    => (bool) $paymentSettings->test_mode,
            'MerchantID'  => $paymentSettings->merchant_id,
            'EncryptType' => '1',
        ]);
    }

    abstract public function processPayment(Order $order);

    abstract public function handleCallback(Order $order);

    public function getPeriodParams(Order $order): array
    {
        return [
            'ExecTimes'    => $order->subscription->duration,
            'Frequency'    => 1,
            'PeriodType'   => 'M',
            'PeriodAmount' => $order->total_amount,
        ];
    }

    public function getInvoiceParams(Order $order): array
    {
        $items = $order->products->map(function (Product $product) {
            return [
                'name'  => $product->name,
                'word'  => '個',
                'price' => $product->price,
                'count' => $product->pivot->quantity,
            ];
        })->toArray();

        return [
            'InvoiceMark'      => 'Y',
            'RelateNumber'     => $order->id,
            'TaxType'          => 1,
            'InvoiceItemName'  => implode('|', array_column($items, 'name')),
            'InvoiceItemWord'  => implode('|', array_column($items, 'word')),
            'InvoiceItemPrice' => implode('|', array_column($items, 'price')),
            'InvoiceItemCount' => implode('|', array_column($items, 'count')),
            'InvType'          => '07',
            'Print'            => '0',
            'CustomerID'       => $order->consumer->id,
            'CustomerName'     => $order->consumer->name,
            'CustomerEmail'    => $order->consumer->email,
            'CustomerPhone'    => $order->consumer->phone,
            'Donation'         => '0',
            'DelayDay'         => '0',
        ];
    }

    public function getPurchaseParams(Order $order): array
    {
        $items = $order->products->map(function (Product $product) {
            return [
                'name'     => $product->name,
                'unit'     => '個',
                'price'    => $product->price,
                'quantity' => $product->pivot->quantity,
            ];
        })->toArray();

        return [
            'transactionId' => $order->id,
            'amount'        => $order->total_amount,
            'notifyUrl'     => route('payment.notify', $order),
            'returnUrl'     => route('payment.return', $order),
            'description'   => '產品簡介',
            'PaymentType'   => 'aio',
            'items'         => $items,
            'ChoosePayment' => $order->payment->method,
        ];
    }
}
