<?php

namespace App\Services;

use App\Models\Order;
use App\Services\Payment\PaymentStrategy;

class PaymentService
{
    protected PaymentStrategy $paymentStrategy;

    public function setPaymentStrategy(PaymentStrategy $paymentStrategy): self
    {
        $this->paymentStrategy = $paymentStrategy;

        return $this;
    }

    public function handleCallback(Order $order)
    {
        return $this->paymentStrategy->handleCallback($order);
    }

    public function processPayment(Order $order)
    {
        return $this->paymentStrategy->processPayment($order);
    }
}
