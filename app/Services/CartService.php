<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

class CartService
{
    public function getCart(): Cart
    {
        return Cart::firstOrCreate(['session_id' => Session::getId()]);
    }

    public function clearCartProducts(): void
    {
        $this->getCart()->products()->detach();
    }

    public function getCartProducts(): Collection
    {
        return $this->getCart()->products;
    }

    public function addCartProduct(Product $product, int $quantity = 1): bool
    {
        $cart = $this->getCart();

        if ($cart->products()->wherePivot('product_id', $product->id)->exists()) {
            return false;
        }

        $cart->products()->attach($product->id, [
            'price'    => $product->price,
            'quantity' => $quantity,
        ]);

        return true;
    }

    public function removeCartProduct(Product $product): void
    {
        $this->getCart()->products()->detach($product->id);
    }

    public function incrementCartProduct(Product $product): void
    {
        $cart = $this->getCart();
        $cart_product = $cart->products()->wherePivot('product_id', $product->id)->first();

        if (! $cart_product) {
            $this->addCartProduct($product);

            return;
        }

        ++$cart_product->pivot->quantity;
        $cart_product->pivot->save();
    }

    public function decrementCartProduct(Product $product): void
    {
        $cart = $this->getCart();
        $cart_product = $cart->products()->wherePivot('product_id', $product->id)->first();

        if ($cart_product->pivot->quantity <= 1) {
            $this->removeCartProduct($product);

            return;
        }

        --$cart_product->pivot->quantity;
        $cart_product->pivot->save();
    }
}
